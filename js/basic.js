(function () {

    var global = "To jest zmienna z zasiegiem globalnym";
    if (global) {

        var global2 = "zmienna globalna druga";
        let localVar = "zmienna lokalna";
        console.log("Zmienna globalna", global);
        console.log("Zmienna lokalna", localVar);
    }
    console.log(global2);

    // Proba wyswietlenia zmiennej lokalnej - wyleci error
    // console.log(localVar);

    const constVar = "Tekst zmiennej const";
    // Proba zamiany tekstu zmiennej const - wyleci error
    // constVar = "Proba zamiany tekstu zmiennej const";
    console.log("Zmienna const", constVar);

    const tablica = [];
    tablica.push("a");
    tablica.push(1);
    tablica.push({
        name: 'Marek',
        surname: 'Sobieraj'
    });

    console.log("Wyswietlenie zawartosci tablicy:");
    for (let i = 0; i < tablica.length; i++) {
        console.log(tablica[i]);
    }

    const lastElement = tablica.pop();
    console.log("Ostatni element z tablicy", lastElement);

    const lastElement2 = tablica.pop();
    console.log("Ostatni element z tablicy", lastElement2);

    // deklaracja funkcji
    const referenceFunction = function (name) {
        console.log("[Wywolanie funkcji] " + name);
    };
    // przykład definicji funkcji z argumentami i returnem
    function add(x, y) {
        return x + y;
    }

    console.log("Przyklad wywolania funkcji");
    referenceFunction();

    //deklaracja obiektow
    let emptyObject = {};
    let exampleObject = {
        name: 'Marek',
        surname: 'Sobieraj',
        cars: true
    };
    //mozliwosc dopisywania do obiektow
    exampleObject.age = 22;

    //obiekty zagniezdzone
    let deepObject = {
        wlasciciel: 'MS',
        ubrania: {
            buty: 'Zamszowe',
            koszula: {
                kolor: 'zielona',
                dlugiRekaw: false
            }
        }
    };
    // odwolywanie sie do zagniezdzonych obiektow
    console.log(deepObject);
    console.log("Kolor koszuli:", deepObject.ubrania.koszula.kolor);

    // przyklad wykorzystania referencji funkcji
    exampleObject.displayReferenced = referenceFunction;
    exampleObject.displayReferenced("Example usage referenced function");

    console.log("Zmienne niezadeklarowane sa undefined");
    console.log(anotherGlobal);
    var anotherGlobal = "Zmienna globalna zainicjalizowana na samym koncu dokumentu";
})();