(function () {

//    petla for
    console.log("Petla for");

    for (let i = 0; i < 5; i++) {
        console.log("Element " + i);
    }

//    petla while
    while (i > 0) {
        console.log("Element petli while: " + i);
        i--;
    }


//    petla do...while
    do {
        console.log("Wykonaj sie pierw...");
        i++;
    } while (i < 10);
})();